import pandas as pd
import json
import pkg_resources
import tiktoken

class Chatgpt_df():
    def __init__(self,df_params=pd.DataFrame(),ruta_output=""):
        
        self.df_params = df_params
        ruta_output = ruta_output
        ruta_static = pkg_resources.resource_filename(__name__,'static/')
        self.ruta_base_xlsx="{}xlsx/".format(ruta_static)
        self.ruta_base_json="{}json/".format(ruta_static)
        self.json_columns = self.generar_jsonparams()
        # Load the cl100k_base tokenizer which is designed to work with the ada-002 model
        self.tokenizer = tiktoken.get_encoding("cl100k_base")
        self.max_tokens = 500
        
        
    
    def generar_df(self):
        columns = ["text","n_tokens"]
        df_base=pd.DataFrame(columns=columns)
        
        return df_base
    
    def data_df(self,df=pd.DataFrame()):
        df=df.copy()    
        df_output=pd.DataFrame(columns=["text"])
        df_output["n_tokens"] =""     
        for index, row in df.iterrows():
            for name_column in self.json_columns.keys():
                df_output.at[index,"text"]= "{} {} {} ".format(df_output.at[index,"text"],self.json_columns[name_column],df.at[index,name_column])
                df_output.at[index,"n_tokens"]= self.generar_ntokens(index,"text",df_output)
        return  df_output 
    
    def generar_ntokens(self,index=0,name="",df=pd.DataFrame()): 
        tokens = self.tokenizer.encode(df.at[index,name]) 
        if len(tokens)==0:
            tokens="0"
        
        return  tokens 
        
    def generar_jsonparams(self,name_xlsx=""):
        ruta_json="{}params_data.json".format(self.ruta_base_json)
        if len(name_xlsx)>0:
            ruta_xlsx="{}{}".format(self.ruta_base_xlsx,name_xlsx)
        else:
            ruta_xlsx="{}{}".format(self.ruta_base_xlsx,"data_clientes_json.xlsx")
        df = pd.read_excel(ruta_xlsx)
        json_data_r = {}
        for column in df.columns:
            json_data_r[column] = df[column][0]
        with open(ruta_json, 'w', encoding="utf-8") as file:
            json.dump(json_data_r, file, indent=4, ensure_ascii=False)
        return json_data_r
    
    def split_into_many(self,text):
    
        # Split the text into sentences
        sentences = text.split('. ')

        # Get the number of tokens for each sentence
        n_tokens = [len(self.tokenizer.encode(" " + sentence)) for sentence in sentences]
        
        chunks = []
        tokens_so_far = 0
        chunk = []

        # Loop through the sentences and tokens joined together in a tuple
        for sentence, token in zip(sentences, n_tokens):

            # If the number of tokens so far plus the number of tokens in the current sentence is greater 
            # than the max number of tokens, then add the chunk to the list of chunks and reset
            # the chunk and tokens so far
            if tokens_so_far + token > self.max_tokens:
                chunks.append(". ".join(chunk) + ".")
                chunk = []
                tokens_so_far = 0

            # If the number of tokens in the current sentence is greater than the max number of 
            # tokens, go to the next sentence
            if token > self.max_tokens:
                continue

            # Otherwise, add the sentence to the chunk and add the number of tokens to the total
            chunk.append(sentence)
            tokens_so_far += token + 1
            
        # Add the last chunk to the list of chunks
        if chunk:
            chunks.append(". ".join(chunk) + ".")

        return chunks