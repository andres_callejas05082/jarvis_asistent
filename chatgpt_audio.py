import whisper
import gradio as gr 
import time
import warnings
import json
import openai
from gtts import gTTS
import os
from base64 import b64decode
import numpy as np
from scipy.io.wavfile import read as wav_read

from IPython.display import HTML, Audio
import numpy as np
from scipy.io.wavfile import write
from IPython.display import clear_output
#from google.colab.output import eval_js
from js2py import eval_js
import pyaudio
import wave
import requests
import numpy as np
from openai.embeddings_utils import distances_from_embeddings, cosine_similarity
import tiktoken

warnings.filterwarnings("ignore")

# with open('GPT_SECRET_KEY.json') as f:
#     data = json.load(f)
    
class audio_txt:
    def __init__(self) :
        with open('./static/json/config.json') as f:
            self.data = json.load(f)
        if len(self.data["API_KEY"]>0):
            with open('./static/json/params_data.json') as g:
                self.columns_data = json.load(g)
                
        
        openai.api_key = self.data["API_KEY"]
        self.model_w = whisper.load_model("base")
        self.tokenizer = tiktoken.get_encoding("cl100k_base")
        
    def chatgpt_api(self,input_text):
        messages = [
        {"role": "system", "content": "You are a helpful assistant."}]

        if input_text:
            messages.append(
                {"role": "user", "content": input_text},
            )
            chat_completion = openai.ChatCompletion.create(
                model="gpt-3.5-turbo", messages=messages
            )

        reply = chat_completion.choices[0].message.content
        return reply
    
    
    def obtener_respuestachatgpt(self,text):
        texto =text
        out_result = self.chatgpt_api(texto)
        return out_result

    def capturar_audio(self,nombre_archivo, duracion=5, formato=pyaudio.paInt16, canal=1, tasa=44100, tamanio_chunk=1024):
        p = pyaudio.PyAudio()
        stream = p.open(format=formato,
                        channels=canal,
                        rate=tasa,
                        input=True,
                        frames_per_buffer=tamanio_chunk)
        print("Grabando...")
        frames = []
        for i in range(0, int(tasa / tamanio_chunk * duracion)):
            data = stream.read(tamanio_chunk)
            frames.append(data)

        print("Grabación finalizada.")
        stream.stop_stream()
        stream.close()
        p.terminate()
        wf = wave.open(nombre_archivo, 'wb')
        wf.setnchannels(canal)
        wf.setsampwidth(p.get_sample_size(formato))
        wf.setframerate(tasa)
        wf.writeframes(b''.join(frames))
        wf.close()
        resultado = self.transcribir(nombre_archivo)
        salida=self.obtener_respuestachatgpt(resultado.text)
        return salida
    
    
    
    def obtener_transcripcion(self,audio_file_path, model="whisper-1"):
        api_key=self.data["API_KEY"]
        url = "https://api.openai.com/v1/audio/transcriptions"
        headers = {
            "Authorization": "Bearer {}".format(api_key),
        }

        with open(audio_file_path, "rb") as f:
            files = {
                "file": (audio_file_path, f, "audio/mp3"),
                "model": (None, model),
            }

            response = requests.post(url, headers=headers, files=files)

        if response.status_code == 200:
            transcription_data = response.json()
            return transcription_data
        else:
            print(f"Error al obtener la transcripción: {response.status_code}")
            print(response.text)
            return None
        
    def transcribir(self,audio_file_path):
        audio_file= open(audio_file_path, "rb")
        transcript = openai.Audio.transcribe("whisper-1", audio_file)
        return transcript
    
    def texto_a_audio(self,texto, archivo_salida="output.mp3", idioma="es"):
        """
        Convierte el texto dado en audio utilizando gTTS.

        :param texto: Texto que se desea convertir en audio.
        :param archivo_salida: Nombre del archivo de audio a guardar.
        :param idioma: Idioma del texto a convertir.
        """
        print(texto)
        tts = gTTS(texto, lang=idioma)
        tts.save(archivo_salida)
        #os.system(f"mpg321 {archivo_salida}")
        
    def inicio(self):
        validar = "1"
        while validar!="0":
            resultado = self.capturar_audio("audio_grabado.wav", duracion=8)
            self.texto_a_audio(texto=resultado)
            validar =input("Precione valor diferente de 0 para continuar")
     
audioTexto = audio_txt()
resultado = audioTexto.inicio()
# print(resultado)
# audioTexto.texto_a_audio(texto=resultado)
   
