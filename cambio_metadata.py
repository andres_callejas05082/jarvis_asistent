

import os
import pikepdf
from lxml import etree





def cambiar_fecha_creacion(pdf_input, pdf_output, nueva_fecha):
    # Asegurarse de que la nueva fecha tenga el formato correcto (D:AAAAMMDDHHmmSS)
    if not nueva_fecha.startswith('D:') or len(nueva_fecha) != 16:
        raise ValueError("La nueva fecha debe tener el formato 'D:AAAAMMDDHHmmSS'")

    # Leer el archivo PDF de entrada
    # Leer el archivo PDF de entrada
    # Leer el archivo PDF de entrada
    with pikepdf.open(pdf_input) as pdf:
        # Leer y analizar el metadato XMP
        xmp_bytes = bytes(pdf.open_metadata())
        if xmp_bytes.strip():
            xmp = xmp_bytes.decode()
            xmp_root = etree.fromstring(xmp)
        else:
            xmp_root = etree.Element('{adobe:ns:meta/}xmpmeta')
            xmp_root.set('{http://www.w3.org/2000/xmlns/}xmp', 'http://ns.adobe.com/xap/1.0/')

        # Espacios de nombres XMP para fechas de creación y modificación
        xmp_ns = {'xmp': 'http://ns.adobe.com/xap/1.0/'}

        # Cambiar la fecha de creación y modificación en la metadata XMP
        for node in xmp_root.xpath('//xmp:CreateDate', namespaces=xmp_ns):
            node.text = nueva_fecha
        for node in xmp_root.xpath('//xmp:ModifyDate', namespaces=xmp_ns):
            node.text = nueva_fecha

        # Guardar el metadato XMP actualizado en el PDF
        # Guardar el metadato XMP actualizado en el PDF
        with pdf.open_metadata(update=True) as metadata:
            metadata.load_from_data(etree.tostring(xmp_root))

        # Guardar el archivo PDF de salida
        pdf.save(pdf_output)
        
# Ejemplo de uso:
pdf_input = 'CERTIFICADO DE ORIGEN KIESERITA.pdf'
pdf_output = 'CERTIFICADO DE ORIGEN KIESERITA2.pdf'
nueva_fecha = 'D:20220217125700'  # Formato: D:AAAAMMDDHHmmSS
cambiar_fecha_creacion(pdf_input, pdf_output, nueva_fecha)