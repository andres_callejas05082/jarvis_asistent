from helper.helper import Helper
import pandas as pd


hp = Helper(username="", dsn="impala-prod") 

query = 'select * from resultados_vspc_clientes.master_customer_data where ingestion_year = 2023 and ingestion_month = 3 and ingestion_day = 30 and estado_cli = "ACTIVO" limit 10000'

df = hp.obtener_dataframe(query)
df.to_excel("data_clientes.xlsx")